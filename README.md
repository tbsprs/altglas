[![CircleCI](https://circleci.com/bb/tbsprs/altglas.svg?style=svg)](https://circleci.com/bb/tbsprs/altglas) ![VersionEye][versioneye-badge] 
[![AGPL license][agpl-license-badge]][agpl3]


# Altglas

The application shows recycling containers for glass on a map.

![Altglas map view][screenshot1] ![Altglas about screen][screenshot2]


[![Available for Android at Google Play][googleplaybadge]][googleplaystore]



## Configuration

Before the app can be built one value has to be set.

* Google Maps v2 API key

These settings can be found here:

* `/app/src/main/res/values/config.xml`



## Author

* Tobias Preuss



## Licenses

The source code is licensed under the [GNU Affero General Public License, Version 3][agpl3].

Texts, image assets such as icons or logos,
if not stated otherwise, are licensed under the
[Creative Commons Attribution 4.0 International License][ccby40].



[screenshot1]: img/altglas-01.png
[screenshot2]: img/altglas-02.png
[googleplaybadge]: img/google-play-badge.png
[googleplaystore]: https://play.google.com/store/apps/details?id=de.avpptr.android.altglas
[agpl3]: http://www.gnu.org/licenses/agpl-3.0.html
[ccby40]: https://creativecommons.org/licenses/by/4.0/
[agpl-license-badge]: http://img.shields.io/badge/license-AGPL--3.0-lightgrey.svg
[versioneye-badge]: https://www.versioneye.com/user/projects/545e4c3d2dc2d9ea68006f58/badge.svg
