package de.avpptr.android.altglas.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;

import de.avpptr.android.altglas.data.api.ApiModule;
import de.avpptr.android.altglas.data.api.ContainersService;
import de.avpptr.android.altglas.models.Container;
import de.avpptr.android.altglas.models.ContainerCharlottenburg;
import de.avpptr.android.altglas.models.ContainersCharlottenburg;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class ContainersServiceTests {

    private static final String SAMPLE_JSON = "" +
            "{\n" +
            "  \"index\": [\n" +
            "    {\n" +
            "      \"alte_nr_\": \"W 1\",\n" +
            "      \"aufsteller\": \"Berlin Recycling\",\n" +
            "      \"braun\": \"1\",\n" +
            "      \"breitengrad\": \"52,48264\",\n" +
            "      \"bunt\": \"\",\n" +
            "      \"gruen\": \"1\",\n" +
            "      \"id\": \"1\",\n" +
            "      \"laengengrad\": \"13,31822\",\n" +
            "      \"lfd_nr_\": \"CW 1\",\n" +
            "      \"plz\": \"10713\",\n" +
            "      \"stadtplanadresse\": \"Mecklenburgische Str. 89\",\n" +
            "      \"standort\": \"Aachener Str. Ecke Mecklenburgische Str.\",\n" +
            "      \"weiss\": \"1\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"item\": [],\n" +
            "  \"messages\": {\n" +
            "    \"messages\": [],\n" +
            "    \"success\": null\n" +
            "  },\n" +
            "  \"results\": {\n" +
            "    \"count\": 115,\n" +
            "    \"items_per_page\": null\n" +
            "  }\n" +
            "}";

    private MockWebServer mWebServer;

    @Before
    public void setUp() throws IOException {
        mWebServer = new MockWebServer();
        mWebServer.start();
    }

    @Test
    public void readCharlottenburgContainersWithServerError() throws IOException {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(500);
        Response<ContainersCharlottenburg> response = getResponse(mockResponse);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessful()).isFalse();
        assertThat(response.code()).isEqualTo(500);
    }

    @Test
    public void readCharlottenburgContainersWithEmptyJson() throws IOException {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody("{}");
        Response<ContainersCharlottenburg> response = getResponse(mockResponse);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessful()).isTrue();
        assertThat(response.code()).isEqualTo(200);
    }

    @Test
    public void readCharlottenburgContainersWithSampleJson() throws IOException {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(SAMPLE_JSON);
        Response<ContainersCharlottenburg> response = getResponse(mockResponse);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessful()).isTrue();
        assertThat(response.code()).isEqualTo(200);
        ContainersCharlottenburg containers = response.body();
        assertThat(containers).isNotEmpty();
        assertThat(containers.size()).isEqualTo(1);
        for (Container container : containers) {
            assertThat(container).isNotNull();
            testContainerCharlottenburg((ContainerCharlottenburg) container);
        }
    }

    private void testContainerCharlottenburg(ContainerCharlottenburg container) {
        assertThat(container.id).isEqualTo(0);
        assertThat(container.address).isEqualTo("Mecklenburgische Str. 89");
        assertThat(container.colorBrown).isEqualTo(1);
        assertThat(container.colorGreen).isEqualTo(1);
        assertThat(container.colorWhite).isEqualTo(1);
        assertThat(container.colored).isEqualTo(0);
        assertThat(container.customId).isEqualTo("CW 1");
        assertThat(container.deployer).isEqualTo("Berlin Recycling");
        assertThat(container.deprecatedId).isEqualTo("W 1");
        assertThat(container.latitude).isEqualTo(52.48264f);
        assertThat(container.longitude).isEqualTo(13.31822f);
        assertThat(container.location).isEqualTo("Aachener Str. Ecke Mecklenburgische Str.");
        assertThat(container.uniqueId).isEqualTo(1);
        assertThat(container.zipCode).isEqualTo(10713);
    }

    private Response<ContainersCharlottenburg> getResponse(MockResponse mockResponse) throws IOException {
        ApiModule apiModule = new ApiModule();
        ContainersService containersService = apiModule.provideContainersService(
                mWebServer.url("").toString());
        mWebServer.enqueue(mockResponse);
        Call<ContainersCharlottenburg> call = containersService.readCharlottenburgContainers();
        return call.execute();
    }

    @After
    public void tearDown() throws IOException {
        mWebServer.shutdown();
    }

}
