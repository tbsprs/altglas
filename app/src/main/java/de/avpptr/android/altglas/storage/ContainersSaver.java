package de.avpptr.android.altglas.storage;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import de.avpptr.android.altglas.models.Containers;

public class ContainersSaver extends AsyncTaskLoader<Containers> {

    private final ContainerWriter mContainerWriter;
    private final Containers mContainers;

    public ContainersSaver(Context context, ContainerWriter containerWriter, Containers containers) {
        super(context);
        mContainerWriter = containerWriter;
        mContainers = containers;
        forceLoad();
    }

    @Override
    public Containers loadInBackground() {
        mContainerWriter.storeContainers(mContainers);
        return mContainers;
    }
}
