package de.avpptr.android.altglas.ui;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Marker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import de.avpptr.android.altglas.R;
import de.avpptr.android.altglas.models.ContainerCharlottenburg;
import de.avpptr.android.altglas.models.Containers;
import de.avpptr.android.altglas.prefs.PreferenceHelper;
import de.avpptr.android.altglas.storage.ContainerLoader;
import de.avpptr.android.altglas.storage.ContainerReader;
import de.avpptr.android.altglas.ui.ContainerInfos.ContainerType;

import static de.avpptr.android.altglas.models.ContainerPresence.isPresent;

public class MapFragment extends BaseFragment implements LoaderCallbacks<Containers> {

    private final OnCameraChangeListener mOnCameraChangeListener;
    private final OnMarkerClickListener mOnMarkerClickListener;
    private final OnMapClickListener mOnMapClickListener;
    private ContainerInfos mContainerInfos;

    public MapFragment() {
        mOnCameraChangeListener = new OnCameraChangeListener();
        mOnMarkerClickListener = new OnMarkerClickListener();
        mOnMapClickListener = new OnMapClickListener();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContainerInfos = new ContainerInfos(getActivity());
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    protected void setUpMap() {
        mMap.setOnCameraChangeListener(mOnCameraChangeListener);
        mMap.setOnMarkerClickListener(mOnMarkerClickListener);
        mMap.setOnMapClickListener(mOnMapClickListener);
        mMap.setMyLocationEnabled(true);
        final LoaderManager loaderManager = getActivity().getSupportLoaderManager();
        loaderManager.initLoader(ContainerLoader.LOADER_ID, null, this);
        restoreMapStateOrDefault();
    }

    @Override
    public Loader<Containers> onCreateLoader(int loaderId, Bundle bundle) {
        return new ContainerLoader(getActivity(), getApp().getContainerReader());
    }

    @Override
    public void onLoadFinished(Loader<Containers> loader, Containers containers) {
        if (containers == null) {
            throw new NullPointerException("Containers is null");
        }
        if (containers.isEmpty()) {
            Log.e(getClass().getName(), "Containers is empty");
            return;
        }
        updateMap(containers);
    }

    @Override
    public void onLoaderReset(Loader<Containers> loader) {
        // Nothing to do here.
    }

    private void updateMap(final Containers containers) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.clear();
                MarkerGenerator.addMarkers(mMap, containers);
                ClusteringHelper.updateClustering(getActivity(), mMap);
            }
        });
    }

    private static void animateCamera(final GoogleMap map, final LatLng target, float zoomLevel) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(target).zoom(zoomLevel).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void storeCurrentMapState() {
        final LatLng position = mMap.getCameraPosition().target;
        float zoomLevel = mMap.getCameraPosition().zoom;
        final PreferenceHelper preferenceHelper = getApp().getPreferenceHelper();
        preferenceHelper.storeCurrentPosition(position);
        preferenceHelper.storeZoomLevel(zoomLevel);
    }

    private void zoomInToPosition(final LatLng position) {
        float zoomLevel = mMap.getCameraPosition().zoom + 1;
        animateCamera(mMap, position, zoomLevel);
    }

    private void restoreMapStateOrDefault() {
        final PreferenceHelper preferenceHelper = getApp().getPreferenceHelper();
        if (preferenceHelper.storesCurrentPosition() && preferenceHelper.storesZoomLevel()) {
            float zoomLevel = preferenceHelper.restoreZoomLevel();
            LatLng position = preferenceHelper.restoreCurrentPosition();
            animateCamera(mMap, position, zoomLevel);
        } else {
            // Center of Berlin Charlottenburg-Wilmersdorf
            final LatLng districtCenter = new LatLng(52.498889, 13.284917);
            animateCamera(mMap, districtCenter, 10);
        }
    }

    private void updateContainerInfos(final Marker marker) {
        if (marker == null) {
            mContainerInfos.setVisibility(View.GONE);
            return;
        }

        int databaseId;
        try {
            databaseId = marker.getData();
        } catch (NullPointerException e) {
            mContainerInfos.setVisibility(View.GONE);
            return;
        }

        ContainerReader containerReader = getApp().getContainerReader();
        ContainerCharlottenburg container = (ContainerCharlottenburg) containerReader.getContainer(databaseId);
        if (container == null) {
            throw new NullPointerException(
                    "Container with id = " + databaseId + " does not exist in database.");
        }
        mContainerInfos.setTitle(container.location);
        String deployer = getString(R.string.container_info_label_deployer, container.deployer);
        boolean hasComment = container.hasComment();
        if (hasComment) {
            mContainerInfos.setComment(container.comment);
        }
        mContainerInfos.showComment(hasComment);
        mContainerInfos.setFooter(deployer);
        mContainerInfos.setVisibility(ContainerType.BROWN, isPresent(container.colorBrown));
        mContainerInfos.setVisibility(ContainerType.GREEN, isPresent(container.colorGreen));
        mContainerInfos.setVisibility(ContainerType.WHITE, isPresent(container.colorWhite));
        mContainerInfos.setVisibility(ContainerType.COLORED, isPresent(container.colored));
        mContainerInfos.setVisibility(View.VISIBLE);
    }

    public void refreshData() {
        final LoaderManager loaderManager = getActivity().getSupportLoaderManager();
        loaderManager.restartLoader(ContainerLoader.LOADER_ID, null, this);
    }

    private class OnCameraChangeListener implements GoogleMap.OnCameraChangeListener {

        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            storeCurrentMapState();
        }
    }

    private class OnMarkerClickListener implements GoogleMap.OnMarkerClickListener {
        @Override
        public boolean onMarkerClick(Marker marker) {
            if (marker == null) {
                return false;
            }
            if (marker.isCluster()) {
                zoomInToPosition(marker.getPosition());
            }
            updateContainerInfos(marker);
            return true;
        }
    }

    private class OnMapClickListener implements GoogleMap.OnMapClickListener {
        @Override
        public void onMapClick(LatLng latLng) {
            mContainerInfos.setVisibility(View.GONE);
        }
    }

}
