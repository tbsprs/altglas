package de.avpptr.android.altglas.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.model.LatLng;

import de.avpptr.android.altglas.contract.PreferencesKeys;
import info.metadude.android.typedpreferences.BooleanPreference;
import info.metadude.android.typedpreferences.FloatPreference;
import info.metadude.android.typedpreferences.StringPreference;

public class PreferenceHelper {

    private final SharedPreferences mSharedPreferences;

    public PreferenceHelper(final Context context) {
        mSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void storeSetupComplete(boolean setupComplete) {
        BooleanPreference setupCompletePreference = new BooleanPreference(
                mSharedPreferences, PreferencesKeys.SETUP_COMPLETE);
        setupCompletePreference.set(setupComplete);
    }

    public boolean restoreSetupComplete() {
        BooleanPreference setupCompletePreference = new BooleanPreference(
                mSharedPreferences, PreferencesKeys.SETUP_COMPLETE);
        return setupCompletePreference.get();
    }

    public void storeCurrentPosition(final LatLng position) {
        LatLngPreference currentPositionPreference = new LatLngPreference(
                mSharedPreferences, PreferencesKeys.CURRENT_POSITION);
        currentPositionPreference.set(position);
    }

    public boolean storesCurrentPosition() {
        LatLngPreference currentPositionPreference = new LatLngPreference(
                mSharedPreferences, PreferencesKeys.CURRENT_POSITION);
        return currentPositionPreference.isSet();
    }

    public LatLng restoreCurrentPosition() {
        LatLngPreference currentPositionPreference = new LatLngPreference(
                mSharedPreferences, PreferencesKeys.CURRENT_POSITION);
        return currentPositionPreference.get();
    }

    public void storeZoomLevel(float zoomLevel) {
        FloatPreference zoomLevelPreference = new FloatPreference(
                mSharedPreferences, PreferencesKeys.ZOOM_LEVEL);
        zoomLevelPreference.set(zoomLevel);
    }

    public boolean storesZoomLevel() {
        FloatPreference zoomLevelPreference = new FloatPreference(
                mSharedPreferences, PreferencesKeys.ZOOM_LEVEL);
        return zoomLevelPreference.isSet();
    }

    public float restoreZoomLevel() {
        FloatPreference zoomLevelPreference = new FloatPreference(
                mSharedPreferences, PreferencesKeys.ZOOM_LEVEL);
        return zoomLevelPreference.get();
    }

    public void storeResponseHash(final String responseHash) {
        StringPreference responseHashPreference = new StringPreference(
                mSharedPreferences, PreferencesKeys.RESPONSE_HASH);
        responseHashPreference.set(responseHash);
    }

    public String restoreResponseHash() {
        StringPreference responseHashPreference = new StringPreference(
                mSharedPreferences, PreferencesKeys.RESPONSE_HASH);
        return responseHashPreference.get();
    }

}
