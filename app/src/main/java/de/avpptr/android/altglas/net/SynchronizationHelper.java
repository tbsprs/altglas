package de.avpptr.android.altglas.net;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import de.avpptr.android.altglas.AltglasApplication;
import de.avpptr.android.altglas.contract.Authentication;

public abstract class SynchronizationHelper {

    public static void scheduleSynchronization() {
        Bundle bundle = new Bundle();
        // Performing a sync no matter if it is off
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        // Performing a sync no matter if it is off
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        Account account = new Account(Authentication.ACCOUNT_NAME, Authentication.ACCOUNT_TYPE);
        ContentResolver.requestSync(account, Authentication.AUTHORITY, bundle);
    }

    public static void initializeAccount(final Context context) {
        boolean isNewAccount = false;
        final AltglasApplication app = (AltglasApplication) context.getApplicationContext();
        boolean setupComplete = app.getPreferenceHelper().restoreSetupComplete();

        final String authority = Authentication.AUTHORITY;
        Account account = new Account(Authentication.ACCOUNT_NAME, authority);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        if (accountManager.addAccountExplicitly(account, null, null)) {
            // Inform the system that this account supports sync
            ContentResolver.setIsSyncable(account, authority, Authentication.IS_SYNCABLE);
            // Inform the system that this account is eligible for auto sync when the network is up
            ContentResolver.setSyncAutomatically(account, authority, true);
            isNewAccount = true;
        }
        // Schedule initial synchronization
        if (isNewAccount || !setupComplete) {
            Log.d(SynchronizationHelper.class.getName(), "Scheduling initial synchronization");
            scheduleSynchronization();
            app.getPreferenceHelper().storeSetupComplete(true);
        }
    }

}
