package de.avpptr.android.altglas;

import android.app.Application;

import de.avpptr.android.altglas.data.api.ApiModule;
import de.avpptr.android.altglas.prefs.PreferenceHelper;
import de.avpptr.android.altglas.providers.ContainerProvider;
import de.avpptr.android.altglas.storage.ContainerReader;
import de.avpptr.android.altglas.storage.ContainerWriter;
import de.avpptr.android.altglas.storage.DatabaseReader;
import de.avpptr.android.altglas.storage.DatabaseWriter;

public class AltglasApplication extends Application {

    private ApiModule mApiModule;

    private DatabaseReader mDatabaseReader;
    private DatabaseWriter mDatabaseWriter;
    private ContainerReader mContainerReader;
    private ContainerWriter mContainerWriter;
    private PreferenceHelper mPreferenceHelper;

    private DatabaseReader getDatabaseReader(String authority) {
        if (mDatabaseReader == null) {
            mDatabaseReader = new DatabaseReader(getContentResolver(), authority);
        }
        return mDatabaseReader;
    }

    private DatabaseWriter getDatabaseWriter(String authority) {
        if (mDatabaseWriter == null) {
            mDatabaseWriter = new DatabaseWriter(getContentResolver(), authority);
        }
        return mDatabaseWriter;
    }

    public ContainerReader getContainerReader() {
        if (mContainerReader == null) {
            mContainerReader = new ContainerReader(getDatabaseReader(ContainerProvider.AUTHORITY));
        }
        return mContainerReader;
    }

    public ContainerWriter getContainerWriter() {
        if (mContainerWriter == null) {
            mContainerWriter = new ContainerWriter(getDatabaseWriter(ContainerProvider.AUTHORITY));
        }
        return mContainerWriter;
    }

    public PreferenceHelper getPreferenceHelper() {
        if (mPreferenceHelper == null) {
            mPreferenceHelper = new PreferenceHelper(getApplicationContext());
        }
        return mPreferenceHelper;
    }

    public ApiModule getApiModule() {
        if (mApiModule == null) {
            mApiModule = new ApiModule();
        }
        return mApiModule;
    }

}
