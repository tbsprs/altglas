package de.avpptr.android.altglas.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class GooglePlayServicesErrorDialogFragment extends DialogFragment {

    private static final String TAG = GooglePlayServicesErrorDialogFragment.class.getSimpleName();

    private static final String KEY_STATUS = "status";

    private static GooglePlayServicesErrorDialogFragment newInstance(int status) {
        GooglePlayServicesErrorDialogFragment fragment =
                new GooglePlayServicesErrorDialogFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_STATUS, status);
        fragment.setArguments(args);
        return fragment;
    }

    private static void showDialog(int status, FragmentActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        GooglePlayServicesErrorDialogFragment fragment = newInstance(status);
        fragment.show(fragmentManager, TAG);
    }

    public static boolean showDialogIfNotAvailable(FragmentActivity activity) {
        removeDialog(activity);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        boolean available = status == ConnectionResult.SUCCESS;
        if (!available) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                showDialog(status, activity);
            } else {
                Toast.makeText(activity.getApplication(),
                        "Google Play services not available", Toast.LENGTH_SHORT).show();
            }
        }
        return available;
    }

    private static void removeDialog(FragmentActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TAG);
        if (fragment != null) {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        int status = args.getInt(KEY_STATUS);
        return GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0);
    }

}
