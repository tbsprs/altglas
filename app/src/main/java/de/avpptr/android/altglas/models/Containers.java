package de.avpptr.android.altglas.models;

public interface Containers extends Iterable<Container> {

    int size();

    boolean isEmpty();

    void add(Container container);
}
