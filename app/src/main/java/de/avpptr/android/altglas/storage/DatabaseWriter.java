package de.avpptr.android.altglas.storage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;

public class DatabaseWriter {

    private final ContentResolver mContentResolver;
    private final String mAuthority;

    public DatabaseWriter(ContentResolver contentResolver, String authority) {
        mContentResolver = contentResolver;
        mAuthority = authority;
    }

    public void insert(String tableName, ContentValues values) {
        Uri uri = createUri(tableName);
        mContentResolver.insert(uri, values);
    }

    public void bulkInsert(String tableName, ContentValues[] values) {
        Uri uri = createUri(tableName);
        mContentResolver.bulkInsert(uri, values);
    }

    public void deleteAll(String tableName) {
        Uri uri = createUri(tableName);
        mContentResolver.delete(uri, null, null);
    }

    private Uri createUri(String tableName) {
        return Uri.parse(mAuthority + tableName);
    }

}
