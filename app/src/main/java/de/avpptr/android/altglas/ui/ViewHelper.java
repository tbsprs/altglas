package de.avpptr.android.altglas.ui;

import android.app.Activity;
import android.os.Build;
import android.webkit.WebView;
import android.widget.TextView;

public abstract class ViewHelper {

    public static void setupTextViewSimple(
            final Activity activity, int textViewId, final String text) {
        TextView textView = (TextView) activity.findViewById(textViewId);
        textView.setText(text);
    }

    public static void setupTextViewWeb(final Activity activity, int textViewId, int textId) {
        final WebView view = (WebView) activity.findViewById(textViewId);
        final String text = activity.getString(textId);
        view.loadDataWithBaseURL("about:blank", text, "text/html", "utf-8", "about:blank");
        // Transparent background, see: http://stackoverflow.com/q/5003156/356895
        view.setBackgroundColor(0x00000000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            view.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        }
    }

}
