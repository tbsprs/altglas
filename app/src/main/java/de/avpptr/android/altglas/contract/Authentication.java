package de.avpptr.android.altglas.contract;

public interface Authentication {

    // Account name is displayed in settings
    String ACCOUNT_NAME = "de.avpptr.android.altglas";
    // Must match account_type in authentication.xml and sync_adapter.xml
    // and obviously it also has to match AUTHORITY
    String ACCOUNT_TYPE = "de.avpptr.android.altglas";
    String AUTHORITY = "de.avpptr.android.altglas";
    int IS_SYNCABLE = 1;

}
