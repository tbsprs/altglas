package de.avpptr.android.altglas.contract;

import de.avpptr.android.altglas.BuildConfig;

public interface BundleKeys {

    String NOTIFICATION_MESSAGE = BuildConfig.APPLICATION_ID + ".NOTIFICATION_MESSAGE";

}
