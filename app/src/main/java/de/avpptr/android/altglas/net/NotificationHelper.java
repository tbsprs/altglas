package de.avpptr.android.altglas.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import de.avpptr.android.altglas.BuildConfig;
import de.avpptr.android.altglas.contract.BundleKeys;

public abstract class NotificationHelper {

    public static final String INTENT_ACTION_SYNCHRONIZATION_DONE =
            BuildConfig.APPLICATION_ID + ".INTENT_ACTION_SYNCHRONIZATION_DONE";
    public static final String INTENT_ACTION_SYNCHRONIZATION_ERROR =
            BuildConfig.APPLICATION_ID + ".INTENT_ACTION_SYNCHRONIZATION_ERROR";

    public static void sendNotification(final Context context, final String intentAction) {
        LocalBroadcastManager.getInstance(context)
                .sendBroadcast(new Intent(intentAction));
    }

    public static void sendNotification(final Context context, final String intentAction, int messageResourceId) {
        final String message = context.getString(messageResourceId);
        sendNotification(context, intentAction, message);
    }

    public static void sendNotification(final Context context, final String intentAction, final String message) {
        final Intent intent = new Intent(intentAction);
        intent.putExtra(BundleKeys.NOTIFICATION_MESSAGE, message);
        LocalBroadcastManager.getInstance(context)
                .sendBroadcast(intent);
    }

    public static void registerReceiver(final Context context,
                                        final BroadcastReceiver broadcastReceiver,
                                        final String intentAction) {
        LocalBroadcastManager.getInstance(context)
                .registerReceiver(broadcastReceiver, new IntentFilter(intentAction));
    }

    public static void unregisterReceiver(final Context context,
                                          final BroadcastReceiver broadcastReceiver) {
        LocalBroadcastManager.getInstance(context)
                .unregisterReceiver(broadcastReceiver);
    }

}
