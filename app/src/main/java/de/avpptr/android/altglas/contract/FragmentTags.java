package de.avpptr.android.altglas.contract;

import de.avpptr.android.altglas.BuildConfig;

public interface FragmentTags {

    String FRAGMENT_MAP = BuildConfig.APPLICATION_ID + ".FRAGMENT_MAP";

}
