package de.avpptr.android.altglas.ui;

import android.os.Bundle;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.impl.ExtendedMapFactory;
import com.google.android.gms.maps.GoogleMapOptions;

public class SupportMapFragment extends com.google.android.gms.maps.SupportMapFragment {

    // value taken from google-play-services.jar
    private static final String MAP_OPTIONS = "MapOptions";
    private com.androidmapsextensions.GoogleMap mMap;

    public static SupportMapFragment newInstance() {
        return new SupportMapFragment();
    }

    public static SupportMapFragment newInstance(GoogleMapOptions options) {
        SupportMapFragment fragment = new SupportMapFragment();
        Bundle args = new Bundle();
        args.putParcelable(MAP_OPTIONS, options);
        fragment.setArguments(args);
        return fragment;
    }

    public GoogleMap getExtendedMap() {
        if (mMap == null) {
            com.google.android.gms.maps.GoogleMap realMap = super.getMap();
            if (realMap != null) {
                mMap = ExtendedMapFactory.create(realMap, getActivity());
            }
        }
        return mMap;
    }

}