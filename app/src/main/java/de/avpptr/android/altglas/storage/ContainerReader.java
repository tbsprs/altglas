package de.avpptr.android.altglas.storage;

import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.Log;

import de.avpptr.android.altglas.models.Container;
import de.avpptr.android.altglas.models.ContainerCharlottenburg;
import de.avpptr.android.altglas.models.Containers;
import de.avpptr.android.altglas.models.ContainersCharlottenburg;
import de.avpptr.android.altglas.providers.ContainerProvider;

public class ContainerReader {

    private final DatabaseReader mDatabaseReader;

    public ContainerReader(DatabaseReader databaseReader) {
        mDatabaseReader = databaseReader;
    }

    public Containers getAll() {
        Cursor cursor = mDatabaseReader.getAllFrom(ContainerProvider.TABLE_NAME_CONTAINERS);
        Containers containers = populateListWith(cursor);
        cursor.close();
        return containers;
    }

    public Container getContainer(int primaryKey) {
        Cursor cursor = mDatabaseReader.getFrom(ContainerProvider.TABLE_NAME_CONTAINERS, primaryKey);
        if (cursor.moveToFirst()) {
            Container container = getContainer(cursor);
            cursor.close();
            return container;
        }
        return null;
    }

    private Containers populateListWith(Cursor cursor) {
        Containers data = new ContainersCharlottenburg();
        if (cursor.moveToFirst()) {
            do {
                Container container = getContainer(cursor);
                data.add(container);
            } while (cursor.moveToNext());
        } else {
            Log.d(getClass().getName(), "No data in the cursor.");
        }
        return data;
    }

    private Container getContainer(Cursor cursor) {
        final int cId = cursor.getInt(cursor.getColumnIndexOrThrow(
                BaseColumns._ID));
        final String cAddress = cursor.getString(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_ADDRESS));
        final int cColorBrown = cursor.getInt(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_COLOR_BROWN));
        final int cColorGreen = cursor.getInt(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_COLOR_GREEN));
        final int cColorWhite = cursor.getInt(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_COLOR_WHITE));
        final int cColored = cursor.getInt(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_COLORED));
        final String cComment = cursor.getString(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_COMMENT));
        final String cCustomId = cursor.getString(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_CUSTOM_ID));
        final String cDeployer = cursor.getString(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_DEPLOYER));
        final String cDeprecatedId = cursor.getString(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_DEPRECATED_ID));
        final float cLatitude = cursor.getFloat(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_LATITUDE));
        final String cLocation = cursor.getString(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_LOCATION));
        final float cLongitude = cursor.getFloat(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_LONGITUDE));
        final int cUniqueId = cursor.getInt(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_UNIQUE_ID));
        final int cZipCode = cursor.getInt(cursor.getColumnIndexOrThrow(
                ContainerProvider.COL_CONTAINERS_ZIPCODE));

        return new ContainerCharlottenburg() {{
            id = cId;
            address = cAddress;
            colorBrown = cColorBrown;
            colorGreen = cColorGreen;
            colorWhite = cColorWhite;
            colored = cColored;
            comment = cComment;
            customId = cCustomId;
            deployer = cDeployer;
            deprecatedId = cDeprecatedId;
            latitude = cLatitude;
            location = cLocation;
            longitude = cLongitude;
            uniqueId = cUniqueId;
            zipCode = cZipCode;
        }};
    }

}
