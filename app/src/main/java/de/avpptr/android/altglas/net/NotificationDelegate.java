package de.avpptr.android.altglas.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import de.avpptr.android.altglas.contract.BundleKeys;

public class NotificationDelegate {

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            assert action != null;
            if (action.equals(NotificationHelper.INTENT_ACTION_SYNCHRONIZATION_DONE)) {
                final Bundle extras = intent.getExtras();
                assert extras != null;
                if (intent.hasExtra(BundleKeys.NOTIFICATION_MESSAGE)) {
                    final String message = extras.getString(BundleKeys.NOTIFICATION_MESSAGE);
                    assert message != null;
                    handleSuccess(message);
                }
            } else if (action.equals(NotificationHelper.INTENT_ACTION_SYNCHRONIZATION_ERROR)) {
                final Bundle extras = intent.getExtras();
                assert extras != null;
                if (intent.hasExtra(BundleKeys.NOTIFICATION_MESSAGE)) {
                    final String message = extras.getString(BundleKeys.NOTIFICATION_MESSAGE);
                    assert message != null;
                    handleError(message);
                }
            }
        }
    };

    private final Context mContext;

    public NotificationDelegate(final Context context) {
        mContext = context;
    }

    public void enable() {
        NotificationHelper.registerReceiver(mContext, mBroadcastReceiver,
                NotificationHelper.INTENT_ACTION_SYNCHRONIZATION_DONE);
        NotificationHelper.registerReceiver(mContext, mBroadcastReceiver,
                NotificationHelper.INTENT_ACTION_SYNCHRONIZATION_ERROR);
    }

    public void disable() {
        NotificationHelper.unregisterReceiver(mContext, mBroadcastReceiver);
    }

    private void handleSuccess(final String successMessage) {
        if (mContext instanceof NotificationHandler) {
            NotificationHandler handler = (NotificationHandler) mContext;
            handler.onSynchronizationDone(successMessage);
        } else {
            throw new IllegalStateException(
                    "Context class must implement NotificationHandler interface.");
        }
    }

    private void handleError(final String errorMessage) {
        if (mContext instanceof NotificationHandler) {
            NotificationHandler handler = (NotificationHandler) mContext;
            handler.onSynchronizationError(errorMessage);
        } else {
            throw new IllegalStateException(
                    "Context class must implement NotificationHandler interface.");
        }
    }

}
