package de.avpptr.android.altglas.providers;

import android.net.Uri;

import novoda.lib.sqliteprovider.provider.SQLiteContentProviderImpl;

public class ContainerProvider extends SQLiteContentProviderImpl {

    public static final String AUTHORITY =
            "content://de.avpptr.android.altglas/";
    public static final String TABLE_NAME_CONTAINERS = "containers";
    public static final String COL_CONTAINERS_ADDRESS = "address";
    public static final String COL_CONTAINERS_COLOR_BROWN = "colorBrown";
    public static final String COL_CONTAINERS_COLOR_GREEN = "colorGreen";
    public static final String COL_CONTAINERS_COLOR_WHITE = "colorWhite";
    public static final String COL_CONTAINERS_COLORED = "colored";
    public static final String COL_CONTAINERS_COMMENT = "comment";
    public static final String COL_CONTAINERS_CUSTOM_ID = "customId";
    public static final String COL_CONTAINERS_DEPLOYER = "deployer";
    public static final String COL_CONTAINERS_DEPRECATED_ID = "deprecatedId";
    public static final String COL_CONTAINERS_LATITUDE = "latitude";
    public static final String COL_CONTAINERS_LOCATION = "location";
    public static final String COL_CONTAINERS_LONGITUDE = "longitude";
    public static final String COL_CONTAINERS_UNIQUE_ID = "uniqueId";
    public static final String COL_CONTAINERS_ZIPCODE = "zipCode";

    public static final Uri CONTAINERS =
            Uri.parse(AUTHORITY).buildUpon().appendPath(TABLE_NAME_CONTAINERS).build();

}
