package de.avpptr.android.altglas.data.api;

import de.avpptr.android.altglas.BuildConfig;
import de.avpptr.android.altglas.models.ContainersCharlottenburg;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ContainersService {

    @GET(BuildConfig.API_BERLIN_CHARLOTTENBURG_READ_PATH)
    Call<ContainersCharlottenburg> readCharlottenburgContainers();

}
