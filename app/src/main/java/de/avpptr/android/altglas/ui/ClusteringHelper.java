package de.avpptr.android.altglas.ui;

import android.content.Context;
import android.util.Log;

import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.GoogleMap;

public abstract class ClusteringHelper {

    private static final double[] CLUSTER_SIZES =
            new double[]{180, 160, 144, 120, 96};

    private static final int CLUSTER_SIZE_INDEX_ZERO = 0;

    public static void updateClustering(final Context context, final GoogleMap map) {
        updateClustering(context, map, CLUSTER_SIZE_INDEX_ZERO, true);
    }

    public static void updateClustering(final Context context, final GoogleMap map,
                                        int clusterSizeIndex, boolean enabled) {
        if (map == null) {
            Log.e(MapFragment.class.getName(), "Map is null");
            return;
        }
        ClusteringSettings clusteringSettings = new ClusteringSettings();
        clusteringSettings.addMarkersDynamically(true);

        if (enabled) {
            clusteringSettings.clusterOptionsProvider(
                    new RealClusterOptionsProvider(context.getResources()));
            double clusterSize = CLUSTER_SIZES[clusterSizeIndex];
            clusteringSettings.clusterSize(clusterSize);
        } else {
            clusteringSettings.enabled(false);
        }
        map.setClustering(clusteringSettings);
    }

}
