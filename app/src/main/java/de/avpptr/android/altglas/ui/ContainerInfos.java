package de.avpptr.android.altglas.ui;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.avpptr.android.altglas.R;

public class ContainerInfos {

    private final Activity mActivity;

    enum ContainerType {
        BROWN,
        GREEN,
        WHITE,
        COLORED
    }

    public ContainerInfos(final Activity activity) {
        mActivity = activity;
    }

    public void setVisibility(int visibility) {
        LinearLayout linearLayout = (LinearLayout) mActivity.findViewById(R.id.map_container_infos_layout);
        linearLayout.setVisibility(visibility);
    }

    public void setTitle(final String title) {
        TextView titleTextView = (TextView) mActivity.findViewById(R.id.map_container_infos_title);
        titleTextView.setText(title);
    }

    public void setComment(@NonNull final String comment) {
        TextView view = (TextView) mActivity.findViewById(R.id.map_container_infos_comment);
        view.setText(comment);
    }

    public void showComment(boolean show) {
        TextView view = (TextView) mActivity.findViewById(R.id.map_container_infos_comment);
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void setFooter(final String footer) {
        TextView footerTextView = (TextView) mActivity.findViewById(R.id.map_container_infos_footer);
        footerTextView.setText(footer);
    }

    public void setVisibility(ContainerType containerType, boolean isVisible) {
        int containerId = View.NO_ID;
        switch (containerType) {
            case BROWN:
                containerId = R.id.container_brown;
                break;
            case GREEN:
                containerId = R.id.container_green;
                break;
            case WHITE:
                containerId = R.id.container_white;
                break;
            case COLORED:
                containerId = R.id.container_colored;
                break;
        }
        //noinspection ConstantConditions
        if (containerId != View.NO_ID) {
            View containerImage = mActivity.findViewById(containerId);
            containerImage.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

}
