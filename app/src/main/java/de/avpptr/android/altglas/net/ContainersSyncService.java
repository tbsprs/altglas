package de.avpptr.android.altglas.net;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class ContainersSyncService extends Service {

    private static ContainersSyncAdapter mContainersSyncAdapter;
    private final Object mSyncAdapterLock = new Object();

    @Override
    public void onCreate() {
        synchronized (mSyncAdapterLock) {
            if (mContainersSyncAdapter == null) {
                mContainersSyncAdapter = new ContainersSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mContainersSyncAdapter.getSyncAdapterBinder();
    }

}
