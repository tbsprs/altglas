package de.avpptr.android.altglas.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.androidmapsextensions.GoogleMap;

import de.avpptr.android.altglas.AltglasApplication;
import de.avpptr.android.altglas.R;

public abstract class BaseFragment extends Fragment {

    private SupportMapFragment mMapFragment;
    protected GoogleMap mMap;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createMapFragmentIfNeeded();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void createMapFragmentIfNeeded() {
        FragmentManager fragmentManager = getChildFragmentManager();
        mMapFragment = (SupportMapFragment) fragmentManager
                .findFragmentById(R.id.map_container);
        if (mMapFragment == null) {
            mMapFragment = createMapFragment();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.map_container, mMapFragment);
            fragmentTransaction.commit();
        }
    }

    private SupportMapFragment createMapFragment() {
        return SupportMapFragment.newInstance();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = mMapFragment.getExtendedMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    protected abstract void setUpMap();

    protected AltglasApplication getApp() {
        return (AltglasApplication) getActivity().getApplication();
    }

}
