package de.avpptr.android.altglas.ui;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.util.LruCache;

import com.androidmapsextensions.ClusterOptions;
import com.androidmapsextensions.ClusterOptionsProvider;
import com.androidmapsextensions.Marker;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.List;

import de.avpptr.android.altglas.R;

public class RealClusterOptionsProvider implements ClusterOptionsProvider {

    private static final int[] res = {
            R.drawable.m1,
            R.drawable.m2,
            R.drawable.m3,
            R.drawable.m4,
            R.drawable.m5
    };

    private static final int[] forCounts = {
            10,
            100,
            1000,
            10000,
            Integer.MAX_VALUE
    };

    private final Bitmap[] mBaseBitmaps;
    private final LruCache<Integer, BitmapDescriptor> mCache = new LruCache<>(128);

    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Rect mBounds = new Rect();

    private final ClusterOptions mClusterOptions = new ClusterOptions().anchor(0.5f, 0.5f);

    public RealClusterOptionsProvider(Resources resources) {
        int resourcesLength = res.length;
        mBaseBitmaps = new Bitmap[resourcesLength];
        for (int i = 0; i < resourcesLength; i++) {
            mBaseBitmaps[i] = BitmapFactory.decodeResource(resources, res[i]);
        }
        mPaint.setColor(Color.WHITE);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(resources.getDimension(R.dimen.text_size));
    }

    @Override
    public ClusterOptions getClusterOptions(List<Marker> markers) {

        int markersCount = markers.size();
        BitmapDescriptor cachedIcon = mCache.get(markersCount);
        if (cachedIcon != null) {
            return mClusterOptions.icon(cachedIcon);
        }

        Bitmap base;
        int i = 0;
        do {
            base = mBaseBitmaps[i];
        } while (markersCount >= forCounts[i++]);

        Bitmap bitmap = base.copy(Bitmap.Config.ARGB_8888, true);

        String text = String.valueOf(markersCount);
        mPaint.getTextBounds(text, 0, text.length(), mBounds);
        float x = bitmap.getWidth() / 2.0f;
        float y = (bitmap.getHeight() - mBounds.height()) / 2.0f - mBounds.top;

        Canvas canvas = new Canvas(bitmap);
        canvas.drawText(text, x, y, mPaint);

        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);
        mCache.put(markersCount, icon);

        return mClusterOptions.icon(icon);
    }

}
