package de.avpptr.android.altglas.net;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import de.avpptr.android.altglas.AltglasApplication;
import de.avpptr.android.altglas.BuildConfig;
import de.avpptr.android.altglas.R;
import de.avpptr.android.altglas.data.api.ApiModule;
import de.avpptr.android.altglas.data.api.ContainersService;
import de.avpptr.android.altglas.models.Containers;
import de.avpptr.android.altglas.models.ContainersCharlottenburg;
import de.avpptr.android.altglas.storage.ContainerWriter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContainersSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String FETCHING_CONTAINERS_FAILED = "Fetching containers failed: ";

    private ContainersService mContainersService;

    public ContainersSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        init(context);
    }

    private void init(Context context) {
        AltglasApplication application = (AltglasApplication) context.getApplicationContext();
        ApiModule apiModule = application.getApiModule();
        mContainersService = apiModule.provideContainersService(BuildConfig.API_BERLIN_BASE_URL);
    }

    @Override
    public void onPerformSync(Account account,
                              Bundle extras,
                              String authority,
                              ContentProviderClient provider,
                              SyncResult syncResult) {

        fetchContainers();
    }

    private void fetchContainers() {
        Call<ContainersCharlottenburg> call = mContainersService.readCharlottenburgContainers();
        call.enqueue(new Callback<ContainersCharlottenburg>() {
            @Override
            public void onResponse(Call<ContainersCharlottenburg> call,
                                   Response<ContainersCharlottenburg> response) {
                if (response.isSuccessful()) {
                    onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ContainersCharlottenburg> call, Throwable t) {
                if (t instanceof FileNotFoundException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_file_not_found);
                } else if (t instanceof MalformedURLException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_malform_url);
                } else if (t instanceof UnknownHostException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_unknown_host);
                } else if (t instanceof UnrecognizedPropertyException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_unrecognized_property);
                } else if (t instanceof InvalidFormatException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_invalid_format);
                } else if (t instanceof JsonMappingException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_json_mapping_error);
                } else if (t instanceof JsonParseException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_json_parse_error);
                } else if (t instanceof IOException) {
                    Log.e(getClass().getName(), FETCHING_CONTAINERS_FAILED + t);
                    onReadFailure(R.string.message_io_error_httpclient);
                }
            }
        });
    }

    private void onSuccess(@Nullable Containers containers) {
        if (containers == null) {
            Log.e(getClass().getName(), "Failed downloading containers (null).");
            onReadFailure(R.string.message_empty_containers);
            return;
        }
        if (containers.isEmpty()) {
            Log.e(getClass().getName(), "Failed downloading containers (empty).");
            onReadFailure(R.string.message_empty_containers);
            return;
        }
        storeContainers(containers);
        String message = "Successfully downloaded " + containers.size() + " containers.";
        Log.d(getClass().getName(), message);
        NotificationHelper.sendNotification(getContext(),
                NotificationHelper.INTENT_ACTION_SYNCHRONIZATION_DONE, message);
    }

    private void onReadFailure(@StringRes int errorMessage) {
        onReadFailure(getContext().getString(errorMessage));
    }

    private void onReadFailure(@NonNull String errorMessage) {
        NotificationHelper.sendNotification(getContext(),
                NotificationHelper.INTENT_ACTION_SYNCHRONIZATION_ERROR, errorMessage);
    }

    private void storeContainers(@NonNull Containers containers) {
        AltglasApplication altglasApplication =
                (AltglasApplication) getContext().getApplicationContext();
        if (altglasApplication == null) {
            throw new NullPointerException("AltglasApplication is null");
        }
        ContainerWriter containerWriter = altglasApplication.getContainerWriter();
        containerWriter.deleteAllContainers();
        containerWriter.storeContainers(containers);
    }

}
