package de.avpptr.android.altglas.ui;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import de.avpptr.android.altglas.R;
import de.avpptr.android.altglas.contract.FragmentTags;
import de.avpptr.android.altglas.net.NotificationDelegate;
import de.avpptr.android.altglas.net.NotificationHandler;
import de.avpptr.android.altglas.net.SynchronizationHelper;
import de.cketti.library.changelog.ChangeLog;

public class MainActivity extends BaseActivity
        implements NotificationHandler {

    private NotificationDelegate mNotificationDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_container, new PlaceholderFragment())
                    .commit();
        }

        if (savedInstanceState == null) {
            if (GooglePlayServicesErrorDialogFragment.showDialogIfNotAvailable(this)) {
                replaceMainFragment(new MapFragment(), FragmentTags.FRAGMENT_MAP);
            }
        }
        mNotificationDelegate = new NotificationDelegate(this);
        SynchronizationHelper.initializeAccount(this);
        showChangeLogDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNotificationDelegate.enable();
    }

    @Override
    protected void onPause() {
        mNotificationDelegate.disable();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh_data) {
            if (!isNetworkAvailable()) {
                showOfflineMessage();
            }
            SynchronizationHelper.scheduleSynchronization();
            return true;
        } else if (id == R.id.action_about) {
            final Intent intent = new Intent(this, AboutActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_changelog) {
            showFullChangeLogDialog();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSynchronizationDone(final String successMessage) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        MapFragment mapFragment = (MapFragment) fragmentManager
                .findFragmentByTag(FragmentTags.FRAGMENT_MAP);
        if (mapFragment != null) {
            mapFragment.refreshData();
        }
        Toast.makeText(this, successMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSynchronizationError(final String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    private void showOfflineMessage() {
        final String message = getString(R.string.message_currently_offline);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private void showChangeLogDialog() {
        final ChangeLog changeLog = new ChangeLog(this);
        if (changeLog.isFirstRun()) {
            changeLog.getLogDialog().show();
        }
    }

    private void showFullChangeLogDialog() {
        final ChangeLog changeLog = new ChangeLog(this);
        changeLog.getFullLogDialog().show();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container, false);
        }
    }

}
