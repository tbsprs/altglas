package de.avpptr.android.altglas.contract;

import de.avpptr.android.altglas.BuildConfig;

public interface PreferencesKeys {

    String SETUP_COMPLETE = BuildConfig.APPLICATION_ID + ".SETUP_COMPLETE";
    String ZOOM_LEVEL = BuildConfig.APPLICATION_ID + ".ZOOM_LEVEL";
    String RESPONSE_HASH = BuildConfig.APPLICATION_ID + ".RESPONSE_HASH";
    String CURRENT_POSITION = BuildConfig.APPLICATION_ID + ".CURRENT_POSITION";

}
