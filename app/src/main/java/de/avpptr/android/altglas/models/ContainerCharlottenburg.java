package de.avpptr.android.altglas.models;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

import de.avpptr.android.altglas.parsers.FlexibleFloatDeserializer;

@SuppressWarnings("unused")
public class ContainerCharlottenburg implements Container {

    @JsonIgnore
    public int id; // Database id
    @JsonProperty("stadtplanadresse")
    public String address;
    @JsonProperty("braun")
    public int colorBrown;
    @JsonProperty("gruen")
    public int colorGreen;
    @JsonProperty("weiss")
    public int colorWhite;
    @JsonProperty("bunt")
    public int colored;
    @JsonProperty("bemerkungen")
    public String comment;
    @JsonProperty("lfd_nr_")
    public String customId;
    @JsonProperty("aufsteller")
    public String deployer;
    @JsonProperty("alte_nr_")
    public String deprecatedId;
    @JsonDeserialize(using = FlexibleFloatDeserializer.class)
    @JsonProperty("breitengrad")
    public float latitude;
    @JsonProperty("standort")
    public String location;
    @JsonDeserialize(using = FlexibleFloatDeserializer.class)
    @JsonProperty("laengengrad")
    public float longitude;
    @JsonProperty("id")
    public int uniqueId;
    @JsonProperty("plz")
    public int zipCode;

    // Note that additionalProperties are not persisted!
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "[" + id + "], "
                + uniqueId +
                ", (" + latitude + ", " + longitude + "), " +
                address +
                ", comment: " + comment +
                ", brown: " + colorBrown +
                ", green: " + colorGreen +
                ", white: " + colorWhite +
                ", colored: " + colored +
                ", additionalProperties: " + additionalProperties;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }

    @Override
    public boolean hasComment() {
        return !TextUtils.isEmpty(comment);
    }

}
