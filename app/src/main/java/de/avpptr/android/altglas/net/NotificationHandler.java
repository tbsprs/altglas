package de.avpptr.android.altglas.net;

public interface NotificationHandler {

    void onSynchronizationDone(final String successMessage);

    void onSynchronizationError(final String errorMessage);

}
