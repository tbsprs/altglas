package de.avpptr.android.altglas.ui;

import android.os.Bundle;
import android.view.Menu;

import de.avpptr.android.altglas.BuildConfig;
import de.avpptr.android.altglas.R;

public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        ViewHelper.setupTextViewSimple(this, R.id.buildVersion, "v." + getBuildVersionName());
        ViewHelper.setupTextViewSimple(this, R.id.buildTime, BuildConfig.BUILD_TIME);
        ViewHelper.setupTextViewSimple(this, R.id.buildVersionCode, "" + BuildConfig.BUILD_VERSION_CODE);
        ViewHelper.setupTextViewSimple(this, R.id.buildHash, BuildConfig.GIT_SHA);

        // Libraries
        ViewHelper.setupTextViewWeb(this, R.id.appinfo_libraries,
                R.string.appinfo_libraries_content);

        ViewHelper.setupTextViewWeb(this, R.id.appinfo_privacy_policy,
                R.string.appinfo_privacy_policy_content);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

}
