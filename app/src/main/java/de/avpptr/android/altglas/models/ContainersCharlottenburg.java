package de.avpptr.android.altglas.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContainersCharlottenburg implements Containers {

    @JsonProperty("index")
    public List<ContainerCharlottenburg> containers;
    @JsonIgnore
    public Object item;
    @JsonIgnore
    public Object messages;
    @JsonIgnore
    public Object results;

    public ContainersCharlottenburg() {
        containers = new ArrayList<>();
    }

    @Override
    public Iterator iterator() {
        return containers.iterator();
    }

    @Override
    public int size() {
        return containers.size();
    }

    @Override
    public boolean isEmpty() {
        return containers.isEmpty();
    }

    @Override
    public void add(Container container) {
        ContainerCharlottenburg c = (ContainerCharlottenburg) container;
        containers.add(c);
    }

}
