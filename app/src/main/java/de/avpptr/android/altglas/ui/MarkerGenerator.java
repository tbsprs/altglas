package de.avpptr.android.altglas.ui;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import de.avpptr.android.altglas.models.Container;
import de.avpptr.android.altglas.models.ContainerCharlottenburg;
import de.avpptr.android.altglas.models.Containers;

public class MarkerGenerator {

    public static void addMarkers(final GoogleMap map, final Containers containers) {
        MarkerOptions options = new MarkerOptions();
        for (Container container : containers) {
            ContainerCharlottenburg containerCharlottenburg = (ContainerCharlottenburg) container;
            float hue = BitmapDescriptorFactory.HUE_YELLOW;
            Marker marker = map.addMarker(options
                    .icon(BitmapDescriptorFactory.defaultMarker(hue))
                    .title(containerCharlottenburg.location)
                    .position(containerCharlottenburg.getPosition())
            );
            marker.setData(containerCharlottenburg.id);
        }
    }

}
