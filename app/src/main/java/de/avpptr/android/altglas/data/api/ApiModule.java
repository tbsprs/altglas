package de.avpptr.android.altglas.data.api;

import android.support.annotation.NonNull;

import de.avpptr.android.altglas.BuildConfig;
import de.avpptr.android.altglas.net.UserAgentInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public final class ApiModule {

    public ContainersService provideContainersService(@NonNull String baseUrl) {
        return createRetrofit(baseUrl)
                .create(ContainersService.class);
    }

    private Retrofit createRetrofit(@NonNull String baseUrl) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addNetworkInterceptor(new UserAgentInterceptor(BuildConfig.USER_AGENT));
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            builder.addNetworkInterceptor(httpLoggingInterceptor);
        }
        OkHttpClient httpClient = builder.build();
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();
    }

}
