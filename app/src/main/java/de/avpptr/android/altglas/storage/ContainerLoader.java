package de.avpptr.android.altglas.storage;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import de.avpptr.android.altglas.models.Containers;

public class ContainerLoader extends AsyncTaskLoader<Containers> {

    public static final int LOADER_ID = 666;
    private final ContainerReader mContainerReader;

    public ContainerLoader(Context context, ContainerReader containerReader) {
        super(context);
        mContainerReader = containerReader;
        forceLoad();
    }

    @Override
    public Containers loadInBackground() {
        return mContainerReader.getAll();
    }

}
