package de.avpptr.android.altglas.prefs;

import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

import info.metadude.android.typedpreferences.DoublePreference;

public class LatLngPreference {

    private final SharedPreferences mPreferences;
    private final String mKeyLatitude;
    private final String mKeyLongitude;

    public LatLngPreference(final SharedPreferences preferences, final String key) {
        mPreferences = preferences;
        mKeyLatitude = key + "_LATITUDE";
        mKeyLongitude = key + "_LONGITUDE";
    }

    public void set(final LatLng position) {
        DoublePreference latitudePreference = new DoublePreference(
                mPreferences, mKeyLatitude);
        DoublePreference longitudePreference = new DoublePreference(
                mPreferences, mKeyLongitude);
        latitudePreference.set(position.latitude);
        longitudePreference.set(position.longitude);
    }

    public LatLng get() {
        DoublePreference latitudePreference = new DoublePreference(
                mPreferences, mKeyLatitude);
        DoublePreference longitudePreference = new DoublePreference(
                mPreferences, mKeyLongitude);
        double latitude = latitudePreference.get();
        double longitude = longitudePreference.get();
        return new LatLng(latitude, longitude);
    }

    public boolean isSet() {
        DoublePreference latitudePreference = new DoublePreference(
                mPreferences, mKeyLatitude);
        DoublePreference longitudePreference = new DoublePreference(
                mPreferences, mKeyLongitude);
        return latitudePreference.isSet() && longitudePreference.isSet();
    }

    public void delete() {
        DoublePreference latitudePreference = new DoublePreference(
                mPreferences, mKeyLatitude);
        DoublePreference longitudePreference = new DoublePreference(
                mPreferences, mKeyLongitude);
        latitudePreference.delete();
        longitudePreference.delete();
    }

}
