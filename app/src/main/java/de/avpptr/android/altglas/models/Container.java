package de.avpptr.android.altglas.models;

import com.google.android.gms.maps.model.LatLng;

public interface Container {

    LatLng getPosition();

    boolean hasComment();

}
