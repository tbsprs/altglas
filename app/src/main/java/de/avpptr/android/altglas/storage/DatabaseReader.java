package de.avpptr.android.altglas.storage;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

public class DatabaseReader {

    private final ContentResolver mContentResolver;
    private final String mAuthority;

    public DatabaseReader(ContentResolver contentResolver, String authority) {
        mContentResolver = contentResolver;
        mAuthority = authority;
    }

    /**
     * (1) Read - generic table support
     */
    protected Cursor getAllFrom(String tableName) {
        Uri uri = createUri(tableName);
        return mContentResolver.query(uri, null, null, null, null);
    }

    /**
     * (2) Read - primary key support
     */
    public Cursor getFrom(String tableName, int primaryKey) {
        Uri uri = createUri(tableName + "/" + primaryKey);
        return mContentResolver.query(uri, null, null, null, null);
    }

    private Uri createUri(String path) {
        return Uri.parse(mAuthority + path);
    }

}
