package de.avpptr.android.altglas.models;

public class ContainerPresence {

    public static boolean isPresent(int containerCount) {
        return containerCount > 0;
    }

}
