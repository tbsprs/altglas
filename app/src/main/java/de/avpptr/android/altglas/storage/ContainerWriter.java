package de.avpptr.android.altglas.storage;

import android.content.ContentValues;

import de.avpptr.android.altglas.models.Container;
import de.avpptr.android.altglas.models.ContainerCharlottenburg;
import de.avpptr.android.altglas.models.Containers;
import de.avpptr.android.altglas.providers.ContainerProvider;

public class ContainerWriter {

    private final DatabaseWriter mDatabaseWriter;

    public ContainerWriter(DatabaseWriter databaseWriter) {
        mDatabaseWriter = databaseWriter;
    }

    public void storeContainers(Containers containers) {
        final ContentValues[] values = new ContentValues[containers.size()];
        int valuesIndex = 0;
        for (Container container : containers) {
            ContainerCharlottenburg typedContainer = (ContainerCharlottenburg) container;
            ContentValues contentValues = getContentValues(typedContainer);
            values[valuesIndex] = contentValues;
            ++valuesIndex;
        }
        mDatabaseWriter.bulkInsert(ContainerProvider.TABLE_NAME_CONTAINERS, values);
    }

    public void deleteAllContainers() {
        mDatabaseWriter.deleteAll(ContainerProvider.TABLE_NAME_CONTAINERS);
    }

    private static ContentValues getContentValues(final ContainerCharlottenburg container) {
        ContentValues contentValues = new ContentValues(13);
        contentValues.put(ContainerProvider.COL_CONTAINERS_ADDRESS, container.address);
        contentValues.put(ContainerProvider.COL_CONTAINERS_COLOR_BROWN, container.colorBrown);
        contentValues.put(ContainerProvider.COL_CONTAINERS_COLOR_GREEN, container.colorGreen);
        contentValues.put(ContainerProvider.COL_CONTAINERS_COLOR_WHITE, container.colorWhite);
        contentValues.put(ContainerProvider.COL_CONTAINERS_COLORED, container.colored);
        contentValues.put(ContainerProvider.COL_CONTAINERS_COMMENT, container.comment);
        contentValues.put(ContainerProvider.COL_CONTAINERS_CUSTOM_ID, container.customId);
        contentValues.put(ContainerProvider.COL_CONTAINERS_DEPLOYER, container.deployer);
        contentValues.put(ContainerProvider.COL_CONTAINERS_DEPRECATED_ID, container.deprecatedId);
        contentValues.put(ContainerProvider.COL_CONTAINERS_LATITUDE, container.latitude);
        contentValues.put(ContainerProvider.COL_CONTAINERS_LOCATION, container.location);
        contentValues.put(ContainerProvider.COL_CONTAINERS_LONGITUDE, container.longitude);
        contentValues.put(ContainerProvider.COL_CONTAINERS_UNIQUE_ID, container.uniqueId);
        contentValues.put(ContainerProvider.COL_CONTAINERS_ZIPCODE, container.zipCode);
        return contentValues;
    }

}
