CREATE TABLE IF NOT EXISTS 'containers' (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    address TEXT,
    colorBrown INTEGER,
    colorGreen INTEGER,
    colorWhite INTEGER,
    colored INTEGER,
    customId TEXT,
    deployer TEXT,
    deprecatedId TEXT,
    uniqueId INTEGER UNIQUE,
    latitude REAL,
    location TEXT,
    longitude REAL,
    zipCode INTEGER
);
