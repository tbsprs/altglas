# Altglas changelog

## [v.0.4.0](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.4.0)

* Published: 2017-05-19

### Features

* Prevent parsing error when new properties are added to the JSON file.
* Show new "bemerkungen" property if present.
* Resolve Lint warnings.
* General project clean up.
* Update various libraries.
* Update Gradle plugin and wrapper.
* Add gradle-versions-plugin to inspect dependency updates.


## [v.0.3.3](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.3.3)

* Published: 2017-02-11

### Features

* Add German privacy policy.
* Add CircleCI.
* Resolve Lint warnings.
* Clean up build config fields.
* Migrate project to Android Studio 2.2.x and Java 8.
* Update various libraries.
* Update Gradle plugin and wrapper.


## [v.0.3.2](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.3.2)

* Published: 2016-03-21

### Features

* Replace AsyncTask with Retrofit, use Use OkHttp.
* Update various libraries.
* Update Gradle plugin and wrapper.


## [v.0.3.1](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.3.1)

* Published: 2015-10-02

### Features

* Avoid crash after synchronization finished.
* Update various libraries.
* Update Gradle plugin and wrapper.
* Enable verbose Lint output.


## [v.0.3.0](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.3.0)

* Published: 2015-06-03

### Features

* Update SDK and support libraries to v.22.
* Customize main application colors.
* Update property name which has been changed on the server.
* Provide individual name for debug build.


## [v.0.2.2](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.2.2)

* Published: 2014-12-07

### Features

* Show message in case of malformed server address.
* Show message in case of an unknown server address.
* Update libraries and project setup (Gradle, build tools).
* Track dependencies with VersionEye.
* Improve preview for layout files.
* Add shadow above container info.


## [v.0.2.1](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.2.1)

* Published: 2014-11-07

### Features

* Bugfix: Adapt to change of API path and format.
* Prefix container deployer string
* Descrease usage of hard-coded package name.
* Show refresh action item if there is room.
* Update libraries and project setup (Gradle, build tools).


## [v.0.2.0](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.2.0)

* Published: 2014-06-29

### Features

* Enable my location button.
* Zoom to position of a marker cluster when clicked.


## [v.0.1.0](https://bitbucket.org/tbsprs/altglas/commits/tag/v.0.1.0)

* Published: 2014-06-25

### Features

* This is the initial release. Have fun!
* It is built for containers in Berlin Charlottenburg-Wilmersdorf.
